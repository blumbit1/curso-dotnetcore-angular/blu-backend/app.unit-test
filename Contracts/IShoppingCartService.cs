﻿using AppTestingDemo.Entity;

namespace AppTestingDemo.Contracts
{
    public interface IShoppingCartService
    {
        // CRUD
        IEnumerable<ShoppingItem> GetAllItems();

        ShoppingItem Add(ShoppingItem newItem);
        
        ShoppingItem GetById(Guid id);
        
        void Remove(Guid id);
    }
}
